const express = require("express");
const router = express.Router();
const auth = require("../auth");

const UserController = require("../controllers/userControllers");

router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
});


// Registration for user
// http://localhost:4000/api/users/register
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


// retrieve all users | for checking only
router.get("/all", (req, res) => {
	UserController.getUsers().then(result => res.send(result));
})


// User Authentication(login)
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});


// change user to admin
router.put("/:userId/changeToAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.changeToAdmin(req.params.userId).then(result => res.send(result))
	} else {
		res.send(`Sorry. Only admins can change user roles.`);
	}

})


// change user(admin) back to user
router.put("/:userId/changeToUser", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.changeToUser(req.params.userId).then(result => res.send(result))
	} else {
		res.send(`Sorry. Only admins can change user roles.`);
	}

})


// checkout an order (user)
router.post("/order", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	UserController.order(data).then(result => res.send(result));
})


// retrieve all orders from admin
router.get("/orders", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		UserController.getAllOrders().then(result => res.send(result));
	} else {
		res.send({ auth: "Order retrieval failed. Only admins can retrieve all orders." })
	}
});


// retrieve authenticated user's orders
//The "auth.verify" acts as a middleware to ensure that the user is logged in before they can get the details of a user
router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from te request headers as an argument

	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result))
} )



module.exports = router;